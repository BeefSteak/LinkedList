#ifndef LIST_ITEM_H
#define LIST_ITEM_H

#include <memory>

class ListItem
{
private:
    int m_data;
    std::unique_ptr<ListItem> m_next;

public:
    ListItem();
    ListItem(const int, std::unique_ptr<ListItem> = nullptr);
    ListItem(ListItem &);

    int data() const;
    std::unique_ptr<ListItem>& nextPtr() { return m_next; }
    bool hasNext() const { return m_next != nullptr; }
    void setData(const int);
    void setNext(std::unique_ptr<ListItem>);
};

inline ListItem::ListItem() {
    setData(0); setNext(nullptr);
}

inline ListItem::ListItem(const int d, std::unique_ptr<ListItem> n) {
    setData(d); setNext(std::move(n));
}

inline ListItem::ListItem(ListItem &li) {
    setData(li.data());
}

inline int ListItem::data() const {
    return m_data;
}

inline void ListItem::setData(const int d) {
    m_data = d;
}

inline void ListItem::setNext(std::unique_ptr<ListItem> n) {
    m_next = std::move(n);
}

#endif // LIST_ITEM_H
