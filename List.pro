TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    listitem.cpp \
    list.cpp

HEADERS += \
    listitem.h \
    list.h
