#include "list.h"
#include <iostream>
using std::cout;
using std::endl;

List::List() : m_head(nullptr), m_tail(nullptr), m_size(0) {
}

void List::insertFirst(const int d) {
    auto head = std::unique_ptr<ListItem>(new ListItem(d));
    head->setNext(std::move(m_head));
    m_head = std::move(head);
    // update tail
    if(m_size == 0) m_tail = m_head.get();
    else if(m_size == 1) m_tail = m_head->nextPtr().get();
    // update size
    ++m_size;
}

void List::insertLast(const int d) {
    auto tail = std::unique_ptr<ListItem>(new ListItem(d));
    // update tail
    if(m_size == 0) {
        m_head = std::move(tail);
        m_tail = m_head.get();
    } else {
        auto temp = tail.get();
        m_tail->setNext(std::move(tail));
        m_tail = temp;
    }

    ++m_size;
}

void List::insertBefore(const int t, const int d) {
    auto prev = m_head.get();
    if(m_size <= 0) return;
    if(m_size <= 1 || prev->data() == t) {
        insertFirst(d);
    } else {
        auto added = std::unique_ptr<ListItem>(new ListItem(d));

        while(prev->hasNext() && prev->nextPtr()->data() != t) {
            prev = prev->nextPtr().get();
        }
        if(prev->hasNext()) {
            added->setNext(std::move(prev->nextPtr()));
            prev->setNext(std::move(added));
            ++m_size;
        }
    }

}

void List::insertAfter(const int t, const int d) {
    auto cur = find(t);
    auto newItem = std::unique_ptr<ListItem>(new ListItem(d));

    if(cur) {
        if(cur->hasNext()) {
            newItem->setNext(std::move(cur->nextPtr()));
            cur->setNext(std::move(newItem));
        } else {
            m_tail = newItem.get();
            cur->setNext(std::move(newItem));
        }

        ++m_size;
    }
}

void List::remove(const int d) {
    if(m_size <= 0) return;
    if(m_size <= 1) {
        if(m_head->data() == d) {
            m_head = nullptr;
            m_tail = nullptr;
        }
    } else {
        auto prev = m_head.get();
        if(prev->data() == d) {
            m_head = std::move(prev->nextPtr());
        } else {
            auto cur = prev->nextPtr().get();
            while(cur->hasNext() && cur->data() != d){
                prev = prev->nextPtr().get();
                cur = cur->nextPtr().get();
            }
            if(cur->data() == d) {
                prev->setNext(std::move(cur->nextPtr()));
                if(!prev->hasNext()) {
                    m_tail = prev;
                }
            }
        }
    }
}

void List::removeFirst() {
    if(m_head) { // if list is not empty
        m_head = std::move(m_head->nextPtr());
        if(!m_head) {
            m_tail = nullptr;
        }
    }
}

void List::removeLast() {
    if(m_head) {
        if(m_size <= 1) {
            m_head = nullptr;
            m_tail = nullptr;
        } else {
            auto prev = m_head.get();
            while(prev->nextPtr()->hasNext()) {
                prev = prev->nextPtr().get();
            }
            prev->nextPtr() = nullptr;
            m_tail = prev;
        }
    }
}

void List::removeBefore(const int t) {
    if(m_size <= 1 || m_head->data() == t) return;

    if(m_head->nextPtr()->data() == t) {
        m_head = std::move(m_head->nextPtr());
    } else {
        auto del = m_head->nextPtr().get(); // prev will be removed
        auto delPrev = m_head.get();
        while(del->hasNext() && del->nextPtr()->data() != t) {
            delPrev = delPrev->nextPtr().get();
            del = del->nextPtr().get();
        }
        delPrev->setNext(std::move(del->nextPtr()));
    }
}



void List::removeAfter(const int t) {
    auto cur = find(t);

    if(cur && cur->hasNext()) {
        cur->setNext(std::move(cur->nextPtr()->nextPtr()));
        if(!cur->hasNext()) {
            m_tail = cur;
        }
    }
}

ListItem* List::find(int d) {
    auto head = m_head.get();
    while (head && head->data() != d) {
        head = head->nextPtr().get();
    }
    return head;
}

void List::show() {
    auto head = m_head.get();
    while(head) {
        std::cout << head->data() << std::endl;
        head = head->nextPtr().get();
    }
}

