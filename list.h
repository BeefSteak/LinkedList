#ifndef LIST_H
#define LIST_H

#include "listitem.h"

class List
{
private:
    std::unique_ptr<ListItem> m_head;
    ListItem *m_tail;
    unsigned m_size;
    ListItem* find(const int);

public:
    List();

    void insertFirst(const int);
    void insertLast(const int);
    void insertBefore(const int, const int);
    void insertAfter(const int, const int);

    void remove(const int);
    void removeFirst();
    void removeLast();
    void removeBefore(const int);
    void removeAfter(const int);

    int head() const;
    int tail() const;
    int size() const { return m_size; }
    bool isEmpty() const { return (m_head == nullptr && m_tail == nullptr); }
    void show();
};

inline int List::head() const {
    if(m_head) return m_head->data();
}

inline int List::tail() const {
    auto it = m_head.get();

    while(it && it->hasNext()) {
        it = it->nextPtr().get();
    }

    if(it) return it->data();
    return 0;
}

#endif // LIST_H
